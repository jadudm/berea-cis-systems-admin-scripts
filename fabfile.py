from fabric.api import *
from cuisine import *
import re
import datetime
import os

KEYSIZE = 4096

env.hosts = [
              'carter.cs.berea.edu'
            ]

class FabricException(Exception):
    pass

def uname():
  run('uname -s')

def hello(user):
  print("hello {0}".format(user))
 
# CONTRACT
# isValidUsername : string -> boolean 
def isValidUsername (username):
  '''make sure its lowercase'''
  #FIXME: check the length of the username?
  if re.match('^[a-z]+$', username):
    #print("Yay")
    return True
  else:
    return False
  
def cleanUpUsername (username):
  '''Strips the white space from the string containing the username'''
  username = username.strip()
  #print ("[" +str(username)+"]")
  return username

def adduser(username = False):
  result = True
  username = cleanUpUsername(username)
  if username:
    if not isValidUsername(username):
      print ("\t[{0}] is not a valid username. Exiting.".format(username))
      result = False
      
    else:
      '''Precondition: All characters in the username should be alphabetical 
         Postcondition: Will create the user's account, if they do not already exist on the server'''
      # FIXME: Remove this if, and outdent.
      if True:
        with settings(abort_exception = FabricException):
          try:
            #The useradd method will not prompt for a password or additional information about them
            #The -m will only create a home directory if one does not already exsist
            # Home directories are mounted, not local, by default.
            # Note the use of the -b to set the base for home directories.
            cmd = [ "useradd",
                    "-m",
                    "--shell",
                    "/bin/bash",
                    "-b",
                    "/mnt/nfs/home",
                    username
                  ]
            
            sudo(" ".join(cmd))
            print ("\t User {0} successfully created.".format(username))
            result = True
          
          except FabricException:
            #An Abort Error is thrown when the cmd fails
            print("\tuseradd failed for user [{0}]; account probably exists".format(username))
            result = False
        
          except Exception, ex:
            #Catches all other errors that might occur 
            print ("\t"+str(ex))
            result = False
        
      else:
        print ("\tHost {0} does not require user accounts".format(env.host_string))
        
  return result

# FIXME: If the simulate functions are going to be authentic, then
# the commands need to be generated in their own function, and that
# function should be called both in the simulation as well as in
# the "real deal."
def adduser_s(username):
  ''' Simulates the adduser function'''
  print("useradd -m {0}".format(username))
  #simulating creating an account for the entered user
      
def adduserlist_s(listname):
  ''' Simulates the adduserlist function'''
  with open(listname, 'r') as users:
    names = users.read().splitlines()
    print("Users: " +str(names))
    for each in names:
      adduser_s(each)

# FIXME: We can have separate fabfiles for separate functions, or 
# break things out into subfiles, and include them into the master
# fabfile. This would be different than user creation/management,
# so I would suggest it goes in a separate file.
def restoreaccount(username):
  ''' For restoring data from backup'''
  print("")

def generatekeyname(username):
  date = datetime.datetime.now()
  # FIXME: The basedir for all user directory operations needs to be broken
  # out somewhere else...
  sshDir = "/mnt/nfs/home/{0}/.ssh".format(username)
  keyname = "{0}/{1}-{2}-{3}{4:02d}{5:02d}".format(sshDir, username, KEYSIZE, date.year, date.month, date.day)
  return keyname

def addkey(username, keyname):
  result = True
  
  '''
  Precondition: The user should already exsist
  Postcondition: Keys should be placed in their home directory
  '''
  try:
    with mode_sudo():
      # FIXME: All directories like this need to be broken out.
      sshDir = "/mnt/nfs/home/{0}/.ssh".format(username)
      #Create an .ssh file in the user's home directory
      # NOTE: This uses the cuisine library function dir_exists.
      print ("Does {0}'s .ssh directory exist: {1}.".format(username, dir_exists (sshDir)))
      if not dir_exists (sshDir): 
        print ("Making .ssh directory in user {0}'s directory.".format(username))
        # print ("Made it here")
        Mkdir = [ "mkdir", sshDir]
    
        sudo(" ".join(Mkdir))
      
        dir_attribs(sshDir, owner = username, group = username)
    
            
    #Change the privledges
    # FIXME: This does not seem to be working.
    #Chown = ["chown ",
    #        str(username)+":"+str(username),
    #        sshDir]
    #sudo(" ".join(Chown))
    
    with mode_sudo():
      dir_attribs (sshDir, owner = username, group = username)
    
    #Create the ssh key and place it in sshDir
    # FIXME: we shouldn't have to pipe echo through... 
    # we'll want to figure out how to get rid of this, because this is
    # exactly what we want to avoid in our fabfiles.
    # FIXME: We can make some of this simpler by running the keygen
    # locally, copying the file to the server... and, then we have the 
    # keys ready for distribution.
    SshKey= [ "echo",
              "-e",
              "'y'|ssh-keygen",
              "-t",
              "rsa",
              "-N",
              "''",
              "-b",
              str(KEYSIZE),
              "-C",
              str(username)+"@berea.edu",
              "-f",
              keyname
              ]
    # Run this as the user, so their keys are their own?
    sudo(" ".join(SshKey)  )
    
    # These keys are under the wrong user.
    with mode_sudo():
      file_attribs (keyname, owner = username, group = username)
      file_attribs ("{0}.pub".format(keyname), owner = username, group = username)
    
    return result
    
  except Exception, ex:
    print("Could not add an ssh key for user: [{0}]".format(username))
    print ("This is ex: " +str(ex))
    result = False
    return result

# FIXME: The cuisine library has tools for this.
# we should probably use them. Also has an "unauthorize" function.
def authorizekey(username, pubkey):
  result = True
  
  pubkey = pubkey + ".pub"
  # FIXME: All directories need to be fixed.
  sshDir = "/mnt/nfs/home/{0}/.ssh".format(username)
  
  authTouch = ["touch",
                 "{0}/authorized_keys".format(sshDir)
               ]
  # The result from sudo here is None.
  result = sudo(" ".join(authTouch))
  
  #print ("sudo result: {0}".format(result))
  
  with mode_sudo():
    file_attribs ("{0}/authorized_keys".format(sshDir), owner = username, group = username)
  
  
  auth = ["cat",
          pubkey,
          ">>",
          "{0}/authorized_keys".format(sshDir)
          ]

  #print ("Running command [{0}]".format(" ".join(auth)))
  result = sudo(" ".join(auth))
  # Another None result
  #print ("sudo result: {0}".format(result))
  
  # FIXME: The sudo command, in some cases, does not help us...
  # we need a better way of making sure what we wanted to happen, does
  # happen. Again, cuisine has some library calls that would help
  # with keys, etc.
  return True

def copyKeyToRoot (pubkey):
  # Create a place to store a local copy of the keys.
  destDir = "/data/generated-keys"
  if not dir_exists(destDir):
    sudo("mkdir -p {0}".format(destDir))
    with mode_sudo():
      # FIXME: The group "superusers" is something I made up just now.
      # It is the group that any admin users + root live in.
      # This makes some shared data things easier, I think. I hope.
      # Either way, it's the solution I put into place. Needs to be
      # broken out and standardized better...
      dir_attribs ("{0}".format(destDir), owner = "root", group = "superusers")
    sudo ("chown -R root:superuser {0}".format(destDir))
    sudo ("chmod g+rw {0}/*".format(destDir))
    
  print ("Copying {0} to {1}".format(pubkey, destDir))
  cmd = ["cp",
        "{0}*".format(pubkey),
        destDir
        ]

  sudo(" ".join(cmd))
  with mode_sudo():
    file_attribs ("{0}/*".format(destDir), owner = "root", group = "superusers")
    sudo("chmod g+rw {0}/{1}".format(destDir, pubkey))
    sudo("chmod g+rw {0}/{1}.pub".format(destDir, pubkey))
  
def fullNewUser (username):
  
  if not user_check (username):
    try: 
      result = adduser(username)
      if result:
        pubkey = generatekeyname(username)
        result = addkey(username, pubkey)
      if result:
        authorizekey(username, pubkey)
    
      copyKeyToRoot (pubkey)
    except Exception, ex:
      print ex

def fullNewUserList (filename):
  '''Precondition: The list must in the directory as fabfile.py
     Postcondition: Will create user's for each person, if they don't exist on the server'''
  with open(filename, 'r') as users:
    names = users.read().splitlines()
    for each in names:
      fullNewUser (each)

def genNewKey (username):
  if user_check(username):
    try:
      pubkey = generatekeyname(username)
      result = addkey(username, pubkey)
      if result:
        copyKeyToRoot(pubkey)
    except Exception, ex:
      print ex

# def addkeylist(filename):
#   with open(filename, 'r') as users:
#     names = users.read().splitlines()
#     #print(names)
#     for each in names:
#       addkey(each)
    

  
  
