import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("userfile", 
                    help = "A file containing a messy list of Berea email addresses.",
                    type = str
                    )
parser.add_argument ("-d", "--destination", 
                     help = "Destination file for usernames",
                     type = str
                     )
args = parser.parse_args()


def splitOnSemicolon (line):
  return line.split(';')

def cleanupFile (filename):
  fp = open(filename, 'r')

  if args.destination:
    of = open(args.destination, 'w')
  
  for line in fp:
    lines = splitOnSemicolon(line)
    for user in lines:
      m = re.match('([a-z]+)@berea.edu', user)
      if m:
        print (m.group(1))
        if args.destination:
          of.write(m.group(1) + "\n") 
  if args.destination:
    of.close()
      
if __name__ == "__main__":
  cleanupFile(args.userfile)
