#!/usr/bin/python

from datetime import datetime
from subprocess import call

import argparse
import contextlib
import ntpath
import os
import re
import shutil
import sys
import zipfile

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


@contextlib.contextmanager
def remember_cwd():
    curdir= os.getcwd()
    try: yield
    finally: os.chdir(curdir)
    
def bundle (pwd, args):
  d = datetime.now()
  s = datetime.strftime(d, "%Y%m%d-%H%M%S")
  rootdir = '_tmp-{0}'.format(s)
  destdir = "files-{0}".format(s)
  
  if args.dir:
    destdir = args.dir
  
  if not os.path.isdir(rootdir):
    os.mkdir(rootdir)
  
  os.mkdir(rootdir + "/" + destdir)
  
  for f in args.files:
    keybasename = ntpath.basename(f)
    print ("Copying {0}".format(keybasename))
    shutil.copyfile(f, "{0}/{1}/{2}".format(rootdir, destdir, keybasename))

  print("In directory: {0}".format(os.getcwd()))
  shutil.make_archive("{0}".format(destdir), 'zip', rootdir)

  # Now, move the zipfile to the user's current directory.
  # shutil.move("{0}/{1}.zip".format(rootdir, destdir), pwd)
  
  # Now, remove the temporary directory
  shutil.rmtree("{0}/{1}".format(rootdir, destdir))
  shutil.rmtree("{0}".format(rootdir))
  
  return "{0}.zip".format(destdir)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("files", nargs = "*")
  parser.add_argument('-d', '--dir', help = "Directory to contain files.")
  parser.add_argument('-o', '--onetime', 
                      help = "Get a one-time download ticket.",
                      action='store_true')
  args = parser.parse_args()

  if len(args.files) < 1:
    print("Need at least one file for zipping!")
    exit()
  
  HERE = os.getcwd()
  file = bundle(HERE, args)
  
  if args.onetime:
    os.chdir(HERE)
    cmd = ['dl-cli', '-r', '~/.dl.rc', file]
    # print(cmd)
    call(cmd)
  